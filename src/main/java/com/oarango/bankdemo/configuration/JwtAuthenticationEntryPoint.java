package com.oarango.bankdemo.configuration;

import com.oarango.bankdemo.presentation.exception.ApiError;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        ApiError apiError = ApiError.builder()
                .message("No autorizado")
                .status(HttpStatus.UNAUTHORIZED)
                .generatedAt(new Date())
                .build();
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, apiError.toString());
    }
}
