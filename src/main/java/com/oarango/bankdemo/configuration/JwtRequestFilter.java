package com.oarango.bankdemo.configuration;

import com.oarango.bankdemo.service.auth.JwtUserDetailsService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {
    private final String HEADER = "Authorization";
    private final String PREFIX = "Bearer ";
    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtUtils jwtUtils;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader(HEADER);
        String username = null;
        if (token != null && token.startsWith(PREFIX)) {
            token = token.substring(PREFIX.length());
            username = jwtUtils.getUsernameFromToken(token);
        }
        if (username != null) {
            createJwtSession(request, username, token);
        }
        filterChain.doFilter(request, response);
    }

    private void createJwtSession(HttpServletRequest request, String username, String token) {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);
            if (jwtUtils.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken jwtToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                jwtToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(jwtToken);
            }
        }
    }
}
