/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.dao.customer;

import com.oarango.bankdemo.model.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

/**
 * @author oarango
 */
@Service
public interface CustomerDaoImpl extends JpaRepository<CustomerEntity, Long>, CustomerDao {

    Optional<CustomerEntity> findByCreatedAt(Date createdAt);

    Optional<CustomerEntity> findByDocument(Long id);

    @Query("SELECT c FROM CustomerEntity c WHERE c.names LIKE %?1% OR c.lastNames LIKE %?1%")
    Optional<CustomerEntity> findByCriteria(String criteria);
}
