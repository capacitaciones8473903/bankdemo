/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.oarango.bankdemo.dao.customer;

import com.oarango.bankdemo.model.entity.CustomerEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author oarango
 */
public interface CustomerDao {

    List<CustomerEntity> findAll();

    Optional<CustomerEntity> findByDocument(Long id);

    Optional<CustomerEntity> findByCriteria(String criteria);

    Optional<CustomerEntity> findByCreatedAt(Date createdAt);

    CustomerEntity save(CustomerEntity customerEntity);
}
