package com.oarango.bankdemo.dao.auth;

import com.oarango.bankdemo.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDaoImpl extends JpaRepository<UserEntity, Long>, UserDao {
    Optional<UserEntity> findByUsername(String username);
}
