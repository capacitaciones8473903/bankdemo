package com.oarango.bankdemo.dao.auth;

import com.oarango.bankdemo.model.entity.UserEntity;

import java.util.Optional;

public interface UserDao {

    Optional<UserEntity> findByUsername(String username);
}
