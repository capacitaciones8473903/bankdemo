/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.oarango.bankdemo.dao.account;

import com.oarango.bankdemo.model.entity.AccountEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author oarango
 */
public interface AccountDao {

    List<AccountEntity> findAll();

    Optional<AccountEntity> findByNumber(Long number);

    AccountEntity save(AccountEntity accountEntity);
}
