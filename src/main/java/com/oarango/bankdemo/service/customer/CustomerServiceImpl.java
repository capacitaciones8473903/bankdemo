/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.service.customer;

import com.oarango.bankdemo.dao.customer.CustomerDao;
import com.oarango.bankdemo.model.domain.customer.Customer;
import com.oarango.bankdemo.model.entity.CustomerEntity;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.oarango.bankdemo.presentation.messages.CustomerMessages.CUSTOMER_ALREADY_EXISTS;
import static com.oarango.bankdemo.presentation.messages.CustomerMessages.CUSTOMER_NOT_FOUND;

/**
 * @author oarango
 */
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerDao dao;

    @Override
    @Transactional(readOnly = true)
    public List<Customer> getAll() {
        List<CustomerEntity> records = dao.findAll();
        List<Customer> customers = new ArrayList<>();
        for (CustomerEntity customerEntity : records) {
            customers.add(Customer.toDomain(customerEntity));
        }
        return customers;
    }

    @Override
    @Transactional(readOnly = true)
    public Customer findByDocument(Long id) {
        CustomerEntity customerEntity = dao.findByDocument(id).orElseGet(() -> CustomerEntity.builder().build());
        return Customer.toDomain(customerEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Customer findByCriteria(String criteria) {
        CustomerEntity customerEntity = dao.findByCriteria(criteria).orElseThrow(() -> new EntityNotFoundException(CUSTOMER_NOT_FOUND));
        return Customer.toDomain(customerEntity);
    }

    @Override
    @Transactional
    public Customer create(Customer customer) {
        if (customer.getId() != null) {
            throw new RuntimeException(CUSTOMER_ALREADY_EXISTS);
        }
        customer.setCreatedAt(new Date());
        CustomerEntity customerEntity = Customer.toEntity(customer);
        customerEntity = dao.save(customerEntity);
        return Customer.toDomain(customerEntity);
    }

    @Override
    @Transactional
    public Customer update(Customer customer) {
        if (customer.getId() == null) {
            throw new RuntimeException(CUSTOMER_NOT_FOUND);
        }
        long date = customer.getCreatedAt().getTime();
        CustomerEntity customerDb = dao.findByCreatedAt(new Date(date)).orElseThrow(() -> new EntityNotFoundException(CUSTOMER_NOT_FOUND));
        customerDb.setCreatedAt(new Date(date));
        customerDb.setDocument(customer.getDocument());
        customerDb.setNames(customer.getNames());
        customerDb.setLastNames(customer.getLastNames());
        customerDb = dao.save(customerDb);
        return Customer.toDomain(customerDb);
    }
}
