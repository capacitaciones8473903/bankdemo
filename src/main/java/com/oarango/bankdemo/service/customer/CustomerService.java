/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.oarango.bankdemo.service.customer;

import com.oarango.bankdemo.model.domain.customer.Customer;

import java.util.List;

/**
 * @author oarango
 */
public interface CustomerService {

    List<Customer> getAll();

    Customer findByDocument(Long id); // Añadido

    Customer findByCriteria(String criteria);

    Customer create(Customer customer);

    Customer update(Customer customer);
}
