package com.oarango.bankdemo.service.user;

import com.oarango.bankdemo.model.domain.auth.User;

import java.util.Optional;

public interface UserService {
    Optional<User> findByUsername(String username);
}
