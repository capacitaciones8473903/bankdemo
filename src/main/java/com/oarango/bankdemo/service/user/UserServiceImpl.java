package com.oarango.bankdemo.service.user;

import com.oarango.bankdemo.dao.auth.UserDao;
import com.oarango.bankdemo.model.domain.auth.User;
import com.oarango.bankdemo.model.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserDao dao;

    @Override
    public Optional<User> findByUsername(String username) {
        Optional<UserEntity> entity = dao.findByUsername(username);
        if (entity.isPresent()) {
            return Optional.of(User.toDomain(entity.get()));
        }
        return Optional.empty();
    }
}
