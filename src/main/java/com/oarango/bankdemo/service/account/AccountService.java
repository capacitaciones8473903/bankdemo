/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.oarango.bankdemo.service.account;

import com.oarango.bankdemo.model.domain.account.Account;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author oarango
 */
public interface AccountService {

    List<Account> getAll();

    Account getByNumber(Long number);

    BigDecimal checkBalance(Long number);

    Account create(Account account);

    Account update(Account account);

    Account transfer(Long origin, Long destiny, BigDecimal amount);
}
