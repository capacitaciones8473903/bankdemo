/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.service.account;

import com.oarango.bankdemo.dao.account.AccountDao;
import com.oarango.bankdemo.model.domain.account.Account;
import com.oarango.bankdemo.model.domain.customer.Customer;
import com.oarango.bankdemo.model.entity.AccountEntity;
import com.oarango.bankdemo.service.customer.CustomerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.oarango.bankdemo.presentation.messages.AccountMessages.ACCOUNT_ALREADY_EXISTS;
import static com.oarango.bankdemo.presentation.messages.AccountMessages.ACCOUNT_NOT_FOUND;
import static com.oarango.bankdemo.presentation.messages.AccountMessages.ACCOUNT_NUMBER_NOT_FOUND;

/**
 * @author oarango
 */
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountDao dao;
    private final CustomerService customerService;

    @Override
    @Transactional(readOnly = true)
    public BigDecimal checkBalance(Long number) {
        return dao.findByNumber(number).orElseThrow(() -> new RuntimeException(ACCOUNT_NOT_FOUND)).getBalance();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Account> getAll() {
        List<AccountEntity> records = dao.findAll();
        List<Account> accounts = new ArrayList<>();
        for (AccountEntity accountEntity : records) {
            accounts.add(Account.toDomain(accountEntity));
        }
        return accounts;
    }

    @Override
    @Transactional(readOnly = true)
    public Account getByNumber(Long number) {
        AccountEntity accountEntity = dao.findByNumber(number).orElseThrow(() -> new EntityNotFoundException(ACCOUNT_NUMBER_NOT_FOUND));
        return Account.toDomain(accountEntity);
    }

    @Override
    @Transactional
    public Account create(Account account) {
        if (account.getId() != null) {
            throw new RuntimeException(ACCOUNT_ALREADY_EXISTS);
        }
        Customer customer = findOrCreateCustomer(account.getCustomer());
        account.setCustomer(customer);
        account.generateNumber();
        AccountEntity accountEntity = Account.toEntity(account);
        accountEntity = dao.save(accountEntity);
        return Account.toDomain(accountEntity);
    }

    @Override
    @Transactional
    public Account update(Account account) {
        if (account.getId() == null) {
            throw new RuntimeException(ACCOUNT_NOT_FOUND);
        }
        AccountEntity accountEntity = Account.toEntity(account);
        accountEntity = dao.save(accountEntity);
        return Account.toDomain(accountEntity);
    }

    @Override
    @Transactional
    public Account transfer(Long origin, Long destiny, BigDecimal amount) {
        Account accountFrom = getByNumber(origin);
        Account accountTo = getByNumber(destiny);

        accountFrom.debit(amount);
        accountTo.credit(amount);

        AccountEntity accountEntityFrom = Account.toEntity(accountFrom);
        AccountEntity accountEntityTo = Account.toEntity(accountTo);
        dao.save(accountEntityTo);
        accountEntityFrom = dao.save(accountEntityFrom);
        accountFrom = Account.toDomain(accountEntityFrom);
        return accountFrom;
    }

    private Customer findOrCreateCustomer(Customer customer) {
        Customer customerDb = customerService.findByDocument(customer.getDocument());
        if (customerDb.getId() == null) {
            return customerService.create(customer);
        }
        return customerDb;
    }
}
