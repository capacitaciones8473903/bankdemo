/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.controller;

import com.oarango.bankdemo.model.domain.customer.Customer;
import com.oarango.bankdemo.presentation.api.ResponseApi;
import com.oarango.bankdemo.presentation.customer.CustomerRequest;
import com.oarango.bankdemo.presentation.customer.CustomerResponse;
import com.oarango.bankdemo.service.customer.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.oarango.bankdemo.presentation.messages.CustomerMessages.CUSTOMERS_NOT_FOUND;
import static com.oarango.bankdemo.presentation.messages.CustomerMessages.CUSTOMER_NOT_FOUND;
import static com.oarango.bankdemo.presentation.messages.CustomerMessages.OPERATION_SUCCESS;

/**
 * @author oarango
 */
@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
@Tag(name = "Cliente", description = "Operaciones sobre los clientes")
public class CustomerController {

    private final CustomerService service;

    @GetMapping
    @Operation(summary = "Obtiene todos los clientes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = OPERATION_SUCCESS,
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ResponseApi.class))}),
            @ApiResponse(responseCode = "404", description = CUSTOMERS_NOT_FOUND,
                    content = @Content)
    })
    public ResponseApi getAll() {
        ResponseApi response = ResponseApi.builder()
                .generatedAt(new Date())
                .build();
        List<Customer> records = service.getAll();
        if (records.isEmpty()) {
            response.setMessage(CUSTOMERS_NOT_FOUND);
            response.setStatus(HttpStatus.NOT_FOUND);
        } else {
            response.setMessage(OPERATION_SUCCESS);
            response.setStatus(HttpStatus.OK);
        }
        List<CustomerResponse> responseList = new ArrayList<>();
        for (Customer record : records) {
            responseList.add(CustomerResponse.toDto(record));
        }
        response.setData(responseList);
        return response;
    }

    @GetMapping("/filter/{criteria}")
    @Operation(summary = "Filtra un cliente por nombre o apellido")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = OPERATION_SUCCESS,
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ResponseApi.class))}),
            @ApiResponse(responseCode = "500", description = CUSTOMER_NOT_FOUND,
                    content = @Content)
    })
    public ResponseApi getByCriteria(@PathVariable String criteria) {
        ResponseApi response = ResponseApi.builder()
                .generatedAt(new Date())
                .build();
        Customer customer = service.findByCriteria(criteria);
        response.setMessage(OPERATION_SUCCESS);
        response.setStatus(HttpStatus.OK);
        CustomerResponse customerResponse = CustomerResponse.toDto(customer);
        response.setData(customerResponse);
        return response;
    }

    @PutMapping
    @Operation(summary = "Actualiza un cliente")
    public ResponseApi update(@RequestBody CustomerRequest request) {
        ResponseApi response = ResponseApi.builder()
                .generatedAt(new Date())
                .build();
        Customer customer = CustomerRequest.toDomain(request);
        customer = service.update(customer);
        CustomerResponse customerResponse = CustomerResponse.toDto(customer);
        response.setMessage(OPERATION_SUCCESS);
        response.setStatus(HttpStatus.ACCEPTED);
        response.setData(customerResponse);
        return response;
    }
}
