/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.controller;

import com.oarango.bankdemo.model.domain.account.Account;
import com.oarango.bankdemo.presentation.account.AccountRecord;
import com.oarango.bankdemo.presentation.account.AccountRequest;
import com.oarango.bankdemo.presentation.account.AccountTransferRequest;
import com.oarango.bankdemo.presentation.api.ResponseApi;
import com.oarango.bankdemo.service.account.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.oarango.bankdemo.presentation.messages.AccountMessages.ACCOUNTS_NOT_FOUND;
import static com.oarango.bankdemo.presentation.messages.AccountMessages.OPERATION_FAILED;
import static com.oarango.bankdemo.presentation.messages.AccountMessages.OPERATION_SUCCESS;

/**
 * @author oarango
 */
@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService service;

    @GetMapping
    public ResponseEntity<ResponseApi> getAll() {
        ResponseApi response = ResponseApi.builder().generatedAt(new Date()).build();
        List<Account> records = service.getAll();
        if (records.isEmpty()) {
            response.setMessage(ACCOUNTS_NOT_FOUND);
            response.setStatus(HttpStatus.NOT_FOUND);
        } else {
            response.setMessage(OPERATION_SUCCESS);
            response.setStatus(HttpStatus.OK);
        }
        List<AccountRecord> responseList = new ArrayList<>();
        for (Account record : records) {
            responseList.add(new AccountRecord(record.getNumber().value(), record.getBalance().value(), record.getType().value(), null).toDto());
        }
        response.setData(responseList);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/number/{number}")
    public ResponseEntity<ResponseApi> getByNumber(@PathVariable Long number) {
        ResponseApi response = ResponseApi.builder().generatedAt(new Date()).build();
        Account record = service.getByNumber(number);
        AccountRecord accountResponse = new AccountRecord(record.getNumber().value(), record.getBalance().value(), record.getType().value(), null).toDto();
        response.setMessage(OPERATION_SUCCESS);
        response.setStatus(HttpStatus.OK);
        response.setData(accountResponse);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/balance/{number}")
    public ResponseEntity<ResponseApi> checkBalance(@PathVariable Long number) {
        ResponseApi response = ResponseApi.builder().generatedAt(new Date()).build();
        BigDecimal balance = service.checkBalance(number);
        response.setMessage(OPERATION_SUCCESS);
        response.setStatus(HttpStatus.OK);
        response.setData(balance);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<ResponseApi> create(@RequestBody AccountRequest request) {
        Account account = AccountRequest.toDomain(request);
        account = service.create(account);
        AccountRecord accountResponse = new AccountRecord(account.getNumber().value(), account.getBalance().value(), account.getType().value(), null).toDto();
        ResponseApi response = ResponseApi.builder().generatedAt(new Date()).build();
        response.setMessage(OPERATION_SUCCESS);
        response.setStatus(HttpStatus.CREATED);
        response.setData(accountResponse);
        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<ResponseApi> update(@RequestBody AccountRequest request) {
        Account account = AccountRequest.toDomain(request);
        account = service.update(account);
        AccountRecord accountResponse = new AccountRecord(account.getNumber().value(), account.getBalance().value(), account.getType().value(), null).toDto();
        ResponseApi response = ResponseApi.builder().generatedAt(new Date()).build();
        if (account.getId() == null) {
            response.setMessage(OPERATION_FAILED);
            response.setStatus(HttpStatus.NOT_MODIFIED);
        } else {
            response.setMessage(OPERATION_SUCCESS);
            response.setStatus(HttpStatus.CREATED);
        }
        response.setData(accountResponse);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/transfer")
    public ResponseEntity<ResponseApi> transfer(@RequestBody AccountTransferRequest request) {
        ResponseApi response = ResponseApi.builder().generatedAt(new Date()).build();
        Account accountFrom = service.transfer(request.getOriginNumber(), request.getDestinyNumber(), request.getAmount());
        AccountRecord accountResponse = new AccountRecord(accountFrom.getNumber().value(), accountFrom.getBalance().value(), accountFrom.getType().value(), null).toDto();
        response.setMessage(OPERATION_SUCCESS);
        response.setStatus(HttpStatus.OK);
        response.setData(accountResponse);
        return ResponseEntity.ok(response);
    }
}
