package com.oarango.bankdemo.model.domain.account;

import com.oarango.bankdemo.model.domain.enums.AccountEnum;
import lombok.Builder;

@Builder
public class Type {
    private final Short type;

    public Type(Short type) {
        if (type == null || type < AccountEnum.AHORROS.value() || type > AccountEnum.CORRIENTE.value()) {
            throw new IllegalArgumentException("El tipo de cuenta debe ser 1 o 2");
        }
        this.type = AccountEnum.AHORROS.value() == type ? AccountEnum.AHORROS.value() : AccountEnum.CORRIENTE.value();
    }

    public Short value() {
        return type;
    }
}
