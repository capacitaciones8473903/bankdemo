package com.oarango.bankdemo.model.domain.account;

import lombok.Builder;

@Builder
public class Number {
    private Long number;

    public Number(Long number) {
        if (number == null) {
            throw new IllegalArgumentException("El número de cuenta no puede ser diferente de null");
        }
        this.number = number;
    }

    public Long value() {
        return number;
    }
}
