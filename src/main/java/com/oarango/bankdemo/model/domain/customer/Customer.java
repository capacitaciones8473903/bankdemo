/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.model.domain.customer;

import com.oarango.bankdemo.model.domain.account.Account;
import com.oarango.bankdemo.model.entity.AccountEntity;
import com.oarango.bankdemo.model.entity.CustomerEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author oarango
 */
@Getter
@Setter
@Builder
public class Customer {

    private Long id;
    private String names;
    private String lastNames;
    private Long document;
    private Date createdAt;

    private List<Account> accounts;

    public static CustomerEntity toEntity(Customer domain) {
        return CustomerEntity.builder()
                .id(domain.getId())
                .names(domain.getNames())
                .lastNames(domain.getLastNames())
                .document(domain.getDocument())
                .createdAt(domain.getCreatedAt())
                .build();
    }

    public static Customer toDomain(CustomerEntity entity) {
        Customer customer = Customer.builder()
                .id(entity.getId())
                .names(entity.getNames())
                .lastNames(entity.getLastNames())
                .document(entity.getDocument())
                .createdAt(entity.getCreatedAt())
                .accounts(new ArrayList<>())
                .build();
        if (entity.getAccounts() != null) {
            for (AccountEntity accountEntity : entity.getAccounts()) {
                Account account = Account.toDomain(accountEntity);
                customer.getAccounts().add(account);
            }
        }
        return customer;
    }
}
