package com.oarango.bankdemo.model.domain.account;

import com.oarango.bankdemo.model.domain.customer.Customer;
import com.oarango.bankdemo.model.entity.AccountEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Account {

    private static final long NUMBER_FACTOR = 10000L;
    private Id id;
    private Number number;
    private Balance balance;
    private Type type;

    private Customer customer;

    public void generateNumber() {
        double number = Math.random();
        number = number * NUMBER_FACTOR;
        number = Math.floor(number);
        this.number = Number.builder().number((long) number).build();
    }

    public void debit(BigDecimal amount) {
        BigDecimal newBalance = balance.value().subtract(amount);
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("El saldo no puede ser negativo");
        }
        balance = Balance.builder().balance(newBalance).build();
    }

    public void credit(BigDecimal amount) {
        BigDecimal newBalance = balance.value().add(amount);
        balance = Balance.builder().balance(newBalance).build();
    }

    public static AccountEntity toEntity(Account domain) {
        return AccountEntity.builder()
                .number(domain.getNumber().value())
                .balance(domain.getBalance().value())
                .type(domain.getType().value())
                .customerEntity(Customer.toEntity(domain.getCustomer()))
                .build();
    }

    public static Account toDomain(AccountEntity entity) {
        return Account.builder()
                .number(Number.builder().number(entity.getNumber()).build())
                .balance(Balance.builder().balance(entity.getBalance()).build())
                .type(Type.builder().type(entity.getType()).build())
                .build();
    }
}
