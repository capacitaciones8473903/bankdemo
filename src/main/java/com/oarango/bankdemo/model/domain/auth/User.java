package com.oarango.bankdemo.model.domain.auth;

import com.oarango.bankdemo.model.entity.UserEntity;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class User {

    private String username;
    private String password;

    public static User toDomain(UserEntity entity) {
        return User.builder()
                .username(entity.getUsername())
                .password(entity.getPassword())
                .build();
    }
}
