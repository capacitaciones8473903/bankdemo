package com.oarango.bankdemo.model.domain.account;

import lombok.Builder;

@Builder
public class Id {
    private Long id;

    public Id(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("El identificador no puede ser nulo");
        }
        this.id = id;
    }

    public Long value() {
        return id;
    }
}
