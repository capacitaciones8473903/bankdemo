package com.oarango.bankdemo.model.domain.account;

import lombok.Builder;

import java.math.BigDecimal;

@Builder
public class Balance {
    private final BigDecimal balance;

    public Balance(BigDecimal balance) {
        if (balance.doubleValue() < 50000) {
            throw new IllegalArgumentException("El saldo inicial debe ser mayor o igual a 50000 pesos");
        }
        this.balance = balance;
    }

    public BigDecimal value() {
        return balance;
    }
}
