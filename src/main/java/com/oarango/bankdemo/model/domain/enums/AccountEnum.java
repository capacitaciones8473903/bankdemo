package com.oarango.bankdemo.model.domain.enums;

public enum AccountEnum {
    AHORROS((short) 1),
    CORRIENTE((short) 2);

    private final short value;

    AccountEnum(short value) {
        this.value = value;
    }

    public short value() {
        return value;
    }
}
