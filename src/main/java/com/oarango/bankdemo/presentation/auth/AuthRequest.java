package com.oarango.bankdemo.presentation.auth;

import com.oarango.bankdemo.model.domain.auth.User;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AuthRequest {
    private String username;
    private String password;

    public static User toDomain(AuthRequest request) {
        return User.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .build();
    }
}
