package com.oarango.bankdemo.presentation.auth;

import java.util.Date;

public record AuthResponse(String token, Date generatedAt, String username) {
}
