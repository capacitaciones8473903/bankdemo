/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * @author oarango
 */
@Getter
@Setter
@ToString
@Builder
public class ApiError {

    private String message;
    private Date generatedAt;
    private HttpStatus status;
}
