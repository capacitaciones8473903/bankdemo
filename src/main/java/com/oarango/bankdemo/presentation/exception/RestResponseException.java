/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.exception;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

/**
 * @author oarango
 */
@ControllerAdvice
public class RestResponseException extends ResponseEntityExceptionHandler {

    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError error = ApiError.builder()
                .generatedAt(new Date())
                .message("Malformed JSON request")
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
        return buildResponseEntity(error);
    }

    @ExceptionHandler(value = {RuntimeException.class, EntityNotFoundException.class})
    protected ResponseEntity<Object> handlerRuntimeException(RuntimeException exception, WebRequest request) {
        ApiError error = ApiError.builder()
                .generatedAt(new Date())
                .message(exception.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
        return buildResponseEntity(error);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    protected ResponseEntity<Object> handlerException(RuntimeException exception, WebRequest request) {
        ApiError error = ApiError.builder()
                .generatedAt(new Date())
                .message(exception.getMessage())
                .status(HttpStatus.BAD_REQUEST)
                .build();
        return buildResponseEntity(error);
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
