/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.account;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author oarango
 */
@Getter
@Setter
public class AccountTransferRequest {

    private Long originNumber;
    private Long destinyNumber;
    private BigDecimal amount;
}
