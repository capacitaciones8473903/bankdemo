package com.oarango.bankdemo.presentation.account;

import java.math.BigDecimal;

public record AccountRecord(Long number, BigDecimal balance, Short type, String typeAccount) {

    public AccountRecord toDto() {
        return new AccountRecord(number, balance, type, type == 1 ? "Ahorros" : "Corriente");
    }
}
