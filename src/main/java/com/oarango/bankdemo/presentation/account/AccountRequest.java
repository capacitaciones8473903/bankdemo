/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.account;

import com.oarango.bankdemo.model.domain.account.Account;
import com.oarango.bankdemo.model.domain.account.Balance;
import com.oarango.bankdemo.model.domain.account.Type;
import com.oarango.bankdemo.presentation.customer.CustomerRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author oarango
 */
@Getter
@Setter
@Builder
public class AccountRequest {
    private BigDecimal balance;
    private Short type;
    private CustomerRequest customerRequest;

    public static Account toDomain(AccountRequest request) {
        return Account.builder()
                .balance(Balance.builder().balance(request.getBalance()).build())
                .type(Type.builder().type(request.getType()).build())
                .customer(CustomerRequest.toDomain(request.getCustomerRequest()))
                .build();
    }
}
