/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.customer;

import com.oarango.bankdemo.model.domain.account.Account;
import com.oarango.bankdemo.model.domain.customer.Customer;
import com.oarango.bankdemo.presentation.account.AccountRecord;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author oarango
 */
@Getter
@Setter
@Builder
public class CustomerResponse {

    private String names;
    private String lastNames;
    private Long document;
    private Date createdAt;
    private List<AccountRecord> accounts;

    public static CustomerResponse toDto(Customer model) {
        CustomerResponse customer = CustomerResponse.builder()
                .names(model.getNames())
                .lastNames(model.getLastNames())
                .document(model.getDocument())
                .createdAt(model.getCreatedAt())
                .accounts(new ArrayList<>())
                .build();
        for (Account account : model.getAccounts()) {
            customer.getAccounts().add(new AccountRecord(account.getNumber().value(), account.getBalance().value(), account.getType().value(), null).toDto());
        }
        return customer;
    }
}
