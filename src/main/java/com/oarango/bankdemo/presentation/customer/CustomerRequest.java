/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.customer;

import com.oarango.bankdemo.model.domain.customer.Customer;
import com.oarango.bankdemo.presentation.account.AccountRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author oarango
 */
@Getter
@Setter
public class CustomerRequest {

    private Long id;
    private String names;
    private String lastNames;
    private Long document;
    private Date createdAt;

    private AccountRequest account;

    public static Customer toDomain(CustomerRequest request) {
        Customer customer = Customer.builder()
                .id(request.getId())
                .names(request.getNames())
                .lastNames(request.getLastNames())
                .document(request.getDocument())
                .createdAt(request.getCreatedAt())
                .accounts(new ArrayList<>())
                .build();
        return customer;
    }
}
