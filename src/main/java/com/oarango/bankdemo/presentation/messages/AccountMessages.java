package com.oarango.bankdemo.presentation.messages;

public class AccountMessages {

    public static final String ACCOUNTS_NOT_FOUND = "No se encontraron cuentas.";
    public static final String OPERATION_SUCCESS = "La operación se ejecutó correctamente.";
    public static final String OPERATION_FAILED = "La operación no se ejecutó correctamente.";
    public static final String ACCOUNT_NOT_FOUND = "No se encontró una cuenta para la consulta";
    public static final String ACCOUNT_NUMBER_NOT_FOUND = "La cuenta con el número indicado no existe.";
    public static final String ACCOUNT_ALREADY_EXISTS = "La cuenta no debe existir.";
}
