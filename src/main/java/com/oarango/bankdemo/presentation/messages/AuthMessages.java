package com.oarango.bankdemo.presentation.messages;

public class AuthMessages {
    public static final String TOKEN_NOT_FOUND = "Token no encontrado";
    public static final String AUTH_FAILED = "El usuario o contraseña no son válidos.";
    public static final String OPERATION_SUCCESS = "La operación se ejecutó correctamente.";
}
