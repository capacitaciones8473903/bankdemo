package com.oarango.bankdemo.presentation.messages;

public class CustomerMessages {
    public static final String CUSTOMER_ALREADY_EXISTS = "El cliente ya existe";
    public static final String CUSTOMER_NOT_FOUND = "El cliente no existe";
    public static final String CUSTOMERS_NOT_FOUND = "No se encontraron clientes";
    public static final String OPERATION_SUCCESS = "La operación se ejecutó correctamente.";
    public static final String OPERATION_FAILED = "La operación no se ejecutó correctamente.";
}
