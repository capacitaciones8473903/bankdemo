/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.oarango.bankdemo.presentation.api;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * @author oarango
 */
@Getter
@Setter
@Builder
public class ResponseApi {

    private String message;
    private Object data;
    private Date generatedAt;
    private HttpStatus status;
}
